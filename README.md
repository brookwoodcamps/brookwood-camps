We are the premier summer sports camp in New York. Former NFL QB Jay Fiedler heads the summer camps New York programming & sleepaway camps in NY.



CONTACT INFORMATION

Location: 574 Proctor Rd. Glen Spey, NY 12737-5569 

Phone: (516) 764-2112

Email: info@brookwoodcamps.com

Contact Person: Jay Fiedler

Website: http://brookwoodcamps.com/

ELSEWHERE ONLINE

https://www.facebook.com/brookwoodcamps/

https://twitter.com/brookwoodcamps

https://plus.google.com/101677626830395360705

https://www.youtube.com/user/brookwoodvideos

https://www.linkedin.com/company/5614279/

